package com.softek.item;

public class ItemChannelListVideo {

    private int ChannelId;
    private String ChannelPlayUrl;
    private String ChannelImage;
    private String ChannelName;
    private String ChannelCatName;


    public int getChannelId() {
        return ChannelId;
    }

    public void setChannelId(int channelId) {
        ChannelId = channelId;
    }

    public String getChannelPlayUrl() {
        return ChannelPlayUrl;
    }

    public void setChannelPlayUrl(String channelPlayUrl) {
        ChannelPlayUrl = channelPlayUrl;
    }

    public String getChannelImage() {
        return ChannelImage;
    }

    public void setChannelImage(String channelImage) {
        ChannelImage = channelImage;
    }

    public String getChannelName() {
        return ChannelName;
    }

    public void setChannelName(String channelName) {
        ChannelName = channelName;
    }

    public String getChannelCatName() {
        return ChannelCatName;
    }

    public void setChannelCatName(String channelCatName) {
        ChannelCatName = channelCatName;
    }

}
