package com.softek.kidstv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.softek.adapter.ChannelAdapter;
import com.softek.item.ItemChannelItem;
import com.softek.item.ItemYTKey;
import com.softek.util.API;
import com.softek.util.BannerAds;
import com.softek.util.Constant;
import com.softek.util.EndlessRecyclerViewScrollListener;
import com.softek.util.IsRTL;
import com.softek.util.ItemOffsetDecoration;
import com.softek.util.JsonUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class NotificationPlayListActivity extends AppCompatActivity {

    ArrayList<ItemChannelItem> mListItem;
    public RecyclerView recyclerView;
    ChannelAdapter adapter;
    private ProgressBar progressBar;
    private LinearLayout lyt_not_found;
    String Id, Name;
    String nextPaegToken, prevPageToken;
    boolean isLoadMore = false, isFirst = true, isFromNotification = false;
    LinearLayoutManager lLayout;
    CardView cardViewError;
    Activity activity;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    MyApplication myApplication;
    ArrayList<ItemYTKey> mListItemKey;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_item);
        IsRTL.ifSupported(NotificationPlayListActivity.this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        JsonUtils.setStatusBarGradiant(NotificationPlayListActivity.this);
        activity = NotificationPlayListActivity.this;
        myApplication = MyApplication.getInstance();
        mListItemKey = new ArrayList<>();

        Intent intent = getIntent();
        Id = intent.getStringExtra("Id");
        Name = intent.getStringExtra("name");
        if (intent.hasExtra("isNotification")) {
            isFromNotification = true;
        }
        setTitle(Name);

        mListItem = new ArrayList<>();
        lyt_not_found = findViewById(R.id.lyt_not_found);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.vertical_courses_list);
        recyclerView.setHasFixedSize(true);
        lLayout = new LinearLayoutManager(NotificationPlayListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(lLayout);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(NotificationPlayListActivity.this, R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);

        cardViewError = findViewById(R.id.card_view_error);
        cardViewError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.COUNT_KEY >= Constant.arrayListKey.size()) {
                    Constant.COUNT_KEY = 0;
                }
                if (JsonUtils.isNetworkAvailable(NotificationPlayListActivity.this)) {
                    new getPlayList().execute(Constant.YTPLAYURL + Id + Constant.YTAPIKEY + Constant.arrayListKey.get(Constant.COUNT_KEY).getKeyName());
                }
            }
        });

        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(lLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (isLoadMore) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (JsonUtils.isNetworkAvailable(NotificationPlayListActivity.this)) {
                                if (Constant.COUNT_KEY >= Constant.arrayListKey.size()) {
                                    Constant.COUNT_KEY = 0;
                                }
                                new getPlayList().execute(Constant.YTPLAYURL + Id + Constant.YTAPIKEY + Constant.arrayListKey.get(Constant.COUNT_KEY).getKeyName() + "&pageToken=" + nextPaegToken);
                            }
                        }
                    }, 1000);
                } else {
                    adapter.hideHeader();
                }
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "get_youtube_key");
        if (JsonUtils.isNetworkAvailable(NotificationPlayListActivity.this)) {
            new MyTaskKey(API.toBase64(jsObj.toString())).execute(Constant.API_URL);
        }

        LinearLayout mAdViewLayout = findViewById(R.id.adView);
        BannerAds.showBannerAds(getApplicationContext(), mAdViewLayout);

    }

    @SuppressLint("StaticFieldLeak")
    private class MyTaskKey extends AsyncTask<String, Void, String> {

        String base64;

        private MyTaskKey(String base64) {
            this.base64 = base64;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0], base64);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (null == result || result.length() == 0) {
                showToast(getString(R.string.nodata));
            } else {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.ARRAY_NAME);
                    JSONObject objJson;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        objJson = jsonArray.getJSONObject(i);

                        ItemYTKey itemKey = new ItemYTKey();
                        itemKey.setKeyId(objJson.getString("id"));
                        itemKey.setKeyName(objJson.getString("youtube_key"));

                        mListItemKey.add(itemKey);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setResultKey();
            }
        }
    }

    private void setResultKey() {
        if (!mListItemKey.isEmpty()) {
            Constant.arrayListKey = mListItemKey;

            if (JsonUtils.isNetworkAvailable(NotificationPlayListActivity.this)) {
                if (Constant.arrayListKey.isEmpty()) {
                    Toast.makeText(NotificationPlayListActivity.this, getString(R.string.error_player_key), Toast.LENGTH_SHORT).show();
                } else {
                    if (Constant.COUNT_KEY >= Constant.arrayListKey.size()) {
                        Constant.COUNT_KEY = 0;
                    }
                    new getPlayList().execute(Constant.YTPLAYURL + Id + Constant.YTAPIKEY + Constant.arrayListKey.get(Constant.COUNT_KEY).getKeyName());
                }
            } else {
                Toast.makeText(NotificationPlayListActivity.this, getString(R.string.conne_msg1), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void showToast(String msg) {
        Toast.makeText(NotificationPlayListActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("StaticFieldLeak")
    private class getPlayList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isFirst)
                showProgress(true);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            return readPlayList(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (isFirst)
                showProgress(false);
            try {
                JSONObject mainJson = new JSONObject(result);
                if (mainJson.has("error")) {
                    cardViewError.setVisibility(View.VISIBLE);
                    isLoadMore = false;
                    endlessRecyclerViewScrollListener.resetState();
                    mListItem.clear();
                    isFirst = true;
                    if (Constant.COUNT_KEY <= Constant.arrayListKey.size()) {
                        Constant.COUNT_KEY++;
                    } else {
                        Constant.COUNT_KEY = 0;
                    }
                    lyt_not_found.setVisibility(View.VISIBLE);
                } else {
                    cardViewError.setVisibility(View.GONE);
                    lyt_not_found.setVisibility(View.GONE);
                    if (mainJson.has("nextPageToken")) {
                        nextPaegToken = mainJson.getString("nextPageToken");
                        isLoadMore = true;
                    } else {
                        isLoadMore = false;
                    }

                    if (mainJson.has("prevPageToken")) {
                        prevPageToken = mainJson.getString("prevPageToken");
                    }

                    JSONObject pageInfo = mainJson.getJSONObject("pageInfo");
                    String totalResults = pageInfo.getString("totalResults");
                    String resultsPerPage = pageInfo.getString("resultsPerPage");

                    JSONArray jsonArray = mainJson.getJSONArray("items");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        ItemChannelItem item = new ItemChannelItem();

                        JSONObject snippet = jsonObject.getJSONObject("snippet");
                        String title = snippet.getString("title");
                        String description = snippet.getString("description");

                        JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                        JSONObject medium = thumbnails.getJSONObject("medium");
                        String url = medium.getString("url");

                        JSONObject id = jsonObject.getJSONObject("id");
                        String videoId = id.getString("videoId");

                        item.setPlaylistId(videoId);
                        item.setPlaylistName(title);
                        item.setPlaylistUserName(description);
                        item.setPlaylistImageurl(url);
                        mListItem.add(item);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isFirst) {
                isFirst = false;
                displayData();
            } else {
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void displayData() {
        adapter = new ChannelAdapter(NotificationPlayListActivity.this, mListItem, recyclerView, Id);
        recyclerView.setAdapter(adapter);
    }

    private void showProgress(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            lyt_not_found.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotificationPlayListActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishAffinity();

    }

    public String readPlayList(String Url) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(Url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
