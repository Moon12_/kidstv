package com.softek.kidstv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ixidev.gdpr.GDPRChecker;
import com.softek.fragment.CategoryFragment;
import com.softek.fragment.FavouriteFragment;
import com.softek.fragment.HomeFragment;
import com.softek.fragment.LatestFragment;
import com.softek.fragment.VideoFragment;
import com.softek.item.ItemAbout;
import com.softek.item.ItemYTKey;
import com.softek.util.API;
import com.softek.util.BannerAds;
import com.softek.util.Constant;
import com.softek.util.IsRTL;
import com.softek.util.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    MyApplication MyApp;
    LinearLayout mAdViewLayout;
    NavigationView navigationView;
    BottomNavigationView bottom_navigation_view;
    int versionCode;
    ArrayList<ItemAbout> mListItem;
    ArrayList<ItemYTKey> mListItemKey;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IsRTL.ifSupported(MainActivity.this);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });
        AudienceNetworkAds.initialize(this);
        JsonUtils.setStatusBarGradiant(MainActivity.this);
        versionCode = BuildConfig.VERSION_CODE;
        mListItem = new ArrayList<>();
        mListItemKey = new ArrayList<>();

        fragmentManager = getSupportFragmentManager();
        MyApp = MyApplication.getInstance();
        navigationView = findViewById(R.id.navigation_view);
        bottom_navigation_view = findViewById(R.id.bottom_navigation_view);
        drawerLayout = findViewById(R.id.drawer_layout);
        mAdViewLayout = findViewById(R.id.adView);

        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "get_app_details");
        if (JsonUtils.isNetworkAvailable(MainActivity.this)) {
            new MyTaskConsent(API.toBase64(jsObj.toString())).execute(Constant.API_URL);
        }

        HomeFragment homeFragment = new HomeFragment();
        fragmentManager.beginTransaction().replace(R.id.Container, homeFragment).commit();

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            drawerLayout.closeDrawers();
            switch (menuItem.getItemId()) {
                case R.id.menu_go_home:
                    bottom_navigation_view.getMenu().findItem(R.id.menu_go_home).setChecked(true);
                    toolbar.setTitle(getString(R.string.menu_home));
                    HomeFragment homeFragment1 = new HomeFragment();
                    fragmentManager.beginTransaction().replace(R.id.Container, homeFragment1).commit();
                    return true;
                case R.id.menu_go_category:
                    bottom_navigation_view.getMenu().findItem(R.id.menu_go_category).setChecked(true);

                    toolbar.setTitle(getString(R.string.menu_category));
                    CategoryFragment currentCategory = new CategoryFragment();
                    fragmentManager.beginTransaction().replace(R.id.Container, currentCategory).commit();
                    return true;
                case R.id.menu_go_latest:
                    bottom_navigation_view.getMenu().findItem(R.id.menu_go_latest).setChecked(true);

                    toolbar.setTitle(getString(R.string.menu_latest));
                    LatestFragment latestFragment = new LatestFragment();
                    fragmentManager.beginTransaction().replace(R.id.Container, latestFragment).commit();
                    return true;
                case R.id.menu_go_video:
                    toolbar.setTitle(getString(R.string.menu_video));
                    VideoFragment videoFragment = new VideoFragment();
                    fragmentManager.beginTransaction().replace(R.id.Container, videoFragment).commit();
                    return true;
                case R.id.menu_go_favourite:
                    bottom_navigation_view.getMenu().findItem(R.id.menu_go_favourite).setChecked(true);

                    toolbar.setTitle(getString(R.string.menu_favourite));
                    FavouriteFragment favouriteFragment = new FavouriteFragment();
                    fragmentManager.beginTransaction().replace(R.id.Container, favouriteFragment).commit();
                    return true;
                case R.id.menu_go_about:
                    Intent about = new Intent(MainActivity.this, AboutUsActivity.class);
                    startActivity(about);
                    return true;
                case R.id.menu_go_rate:
                    RateApp();
                    return true;
                case R.id.menu_go_more:
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.more_app_url))));
                    return true;
                case R.id.menu_go_share:
                    ShareApp();
                    return true;
                case R.id.menu_go_privacy:
                    Intent privacy = new Intent(MainActivity.this, PrivacyActivity.class);
                    startActivity(privacy);
                    return true;
                default:
                    return true;
            }
        });
        bottom_navigation_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_go_home:
                        navigationView.getMenu().findItem(R.id.menu_go_home).setChecked(true);

                        toolbar.setTitle(getString(R.string.menu_home));
                        HomeFragment homeFragment1 = new HomeFragment();
                        fragmentManager.beginTransaction().replace(R.id.Container, homeFragment1).commit();
                        return true;
                    case R.id.menu_go_category:
                        navigationView.getMenu().findItem(R.id.menu_go_category).setChecked(true);

                        toolbar.setTitle(getString(R.string.menu_category));
                        CategoryFragment currentCategory = new CategoryFragment();
                        fragmentManager.beginTransaction().replace(R.id.Container, currentCategory).commit();
                        return true;

                    case R.id.menu_go_latest:
                        navigationView.getMenu().findItem(R.id.menu_go_latest).setChecked(true);

                        toolbar.setTitle(getString(R.string.menu_latest));
                        LatestFragment latestFragment = new LatestFragment();
                        fragmentManager.beginTransaction().replace(R.id.Container, latestFragment).commit();
                        return true;
                    case R.id.menu_go_favourite:
                        navigationView.getMenu().findItem(R.id.menu_go_favourite).setChecked(true);

                        toolbar.setTitle(getString(R.string.menu_video));
                        FavouriteFragment favouriteFragment = new FavouriteFragment();
                        fragmentManager.beginTransaction().replace(R.id.Container, favouriteFragment).commit();
                        return true;
                    case R.id.game:
                        CustomTabsIntent.Builder customIntent = new CustomTabsIntent.Builder();

                        // below line is setting toolbar color
                        // for our custom chrome tab.
                        customIntent.setToolbarColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));

                        // we are calling below method after
                        // setting our toolbar color.
                        openCustomTab(MainActivity.this, customIntent.build(), Uri.parse("https://www.gamezop.com/?id=M6ASQcB0K"));
                        return true;

                    default:
                        return true;
                }
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void highLightNavigation(int position) {
        navigationView.getMenu().getItem(position).setChecked(true);

    }

    public void setToolbarTitle(String Title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(Title);
        }
    }


    private void ShareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_msg) + "\n" + "https://play.google.com/store/apps/details?id=" + getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void RateApp() {
        final String appName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id="
                            + appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + appName)));
        }
    }

    @Override
    public void onBackPressed() {
        ExitApp();
    }

    private void ExitApp() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.exit_msg))
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> finish())
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    // do nothing
                })
                .show();
    }

    @SuppressLint("StaticFieldLeak")
    private class MyTaskConsent extends AsyncTask<String, Void, String> {

        String base64;

        private MyTaskConsent(String base64) {
            this.base64 = base64;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0], base64);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (null == result || result.length() == 0) {
                showToast(getString(R.string.nodata));
            } else {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.ARRAY_NAME);
                    JSONObject objJson;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        objJson = jsonArray.getJSONObject(i);

                        ItemAbout itemAbout = new ItemAbout();

                        itemAbout.setappBannerId(objJson.getString(Constant.ADS_BANNER_ID));
                        itemAbout.setappFullId(objJson.getString(Constant.ADS_FULL_ID));
                        itemAbout.setappBannerOn(objJson.getString(Constant.ADS_BANNER_ON_OFF));
                        itemAbout.setappFullOn(objJson.getString(Constant.ADS_FULL_ON_OFF));
                        itemAbout.setappFullPub(objJson.getString(Constant.ADS_PUB_ID));
                        itemAbout.setappFullAdsClick(objJson.getString(Constant.ADS_CLICK));
                        itemAbout.setAppNativeId(objJson.getString(Constant.NATIVE_AD_ID));
                        itemAbout.setAppNativeType(objJson.getString(Constant.NATIVE_TYPE));
                        itemAbout.setAppNativeOnOff(objJson.getString(Constant.NATIVE_AD_ON_OFF));
                        itemAbout.setAppBannerType(objJson.getString(Constant.BANNER_TYPE));
                        itemAbout.setAppFullType(objJson.getString(Constant.FULL_TYPE));
                        Constant.appUpdateVersion = objJson.getInt("app_new_version");
                        Constant.appUpdateUrl = objJson.getString("app_redirect_url");
                        Constant.appUpdateDesc = objJson.getString("app_update_desc");
                        Constant.isAppUpdate = objJson.getBoolean("app_update_status");
                        Constant.isAppUpdateCancel = objJson.getBoolean("cancel_update_status");
                        Constant.SAVE_NATIVE_CLICK_OTHER = objJson.getString("native_other_position");
                        Constant.SAVE_NATIVE_CLICK_CAT = objJson.getString("native_cat_position");
                        Constant.appPrivacyUrl = objJson.getString("privacy_policy");
                        mListItem.add(itemAbout);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setResult();
            }
        }
    }

    private void setResult() {

        ItemAbout itemAbout = mListItem.get(0);

        Constant.SAVE_ADS_BANNER_ID = itemAbout.getappBannerId();
        Constant.SAVE_ADS_FULL_ID = itemAbout.getappFullId();
        Constant.SAVE_ADS_BANNER_ON_OFF = itemAbout.getappBannerOn();
        Constant.SAVE_ADS_FULL_ON_OFF = itemAbout.getappFullOn();
        Constant.SAVE_ADS_PUB_ID = itemAbout.getappFullPub();
        Constant.SAVE_ADS_CLICK = itemAbout.getappFullAdsClick();
        Constant.SAVE_TAG_LINE = itemAbout.getAppTagLine();
        Constant.SAVE_ADS_NATIVE_ON_OFF = itemAbout.getAppNativeOnOff();
        Constant.SAVE_NATIVE_ID = itemAbout.getAppNativeId();
        Constant.SAVE_BANNER_TYPE = itemAbout.getAppBannerType();
        Constant.SAVE_FULL_TYPE = itemAbout.getAppFullType();
        Constant.SAVE_NATIVE_TYPE = itemAbout.getAppNativeType();

        checkForConsent();
        if (Constant.appUpdateVersion > versionCode && Constant.isAppUpdate) {
            newUpdateDialog();
        }

        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "get_youtube_key");
        if (JsonUtils.isNetworkAvailable(MainActivity.this)) {
            new MyTaskKey(API.toBase64(jsObj.toString())).execute(Constant.API_URL);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class MyTaskKey extends AsyncTask<String, Void, String> {

        String base64;

        private MyTaskKey(String base64) {
            this.base64 = base64;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0], base64);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (null == result || result.length() == 0) {
                showToast(getString(R.string.nodata));
            } else {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.ARRAY_NAME);
                    JSONObject objJson;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        objJson = jsonArray.getJSONObject(i);

                        ItemYTKey itemKey = new ItemYTKey();
                        itemKey.setKeyId(objJson.getString("id"));
                        itemKey.setKeyName(objJson.getString("youtube_key"));

                        mListItemKey.add(itemKey);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setResultKey();
            }
        }
    }

    private void setResultKey() {
        if (!mListItemKey.isEmpty()) {
            Constant.arrayListKey = mListItemKey;
        }
    }

    public void showToast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    public void checkForConsent() {
        new GDPRChecker()
                .withContext(MainActivity.this)
                .withPrivacyUrl(Constant.appPrivacyUrl)
                .withPublisherIds(Constant.SAVE_ADS_PUB_ID)
                .check();
        BannerAds.showBannerAds(MainActivity.this, mAdViewLayout);
    }

    private void newUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.app_update_title));
        builder.setCancelable(false);
        builder.setMessage(Constant.appUpdateDesc);
        builder.setPositiveButton(getString(R.string.app_update_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(Constant.appUpdateUrl)));
            }
        });
        if (Constant.isAppUpdateCancel) {
            builder.setNegativeButton(getString(R.string.app_cancel_btn), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        builder.setIcon(R.mipmap.ic_launcher);
        builder.show();
    }
    public static void openCustomTab(Activity activity, CustomTabsIntent customTabsIntent, Uri uri) {
        // package name is the default package
        // for our custom chrome tab
        String packageName = "com.android.chrome";
        if (packageName != null) {

            // we are checking if the package name is not null
            // if package name is not null then we are calling
            // that custom chrome tab with intent by passing its
            // package name.
            customTabsIntent.intent.setPackage(packageName);

            // in that custom tab intent we are passing
            // our url which we have to browse.
            customTabsIntent.launchUrl(activity, uri);
        } else {
            // if the custom tabs fails to load then we are simply
            // redirecting our user to users device default browser.
            activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }

}
