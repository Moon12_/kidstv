package com.softek.kidstv;

import android.os.Bundle;
import android.view.Window;
import com.softek.util.Constant;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class YtPlayActivity extends YouTubeFailureRecoveryActivity {

    private String PId;
    YouTubePlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yt_play);

        Bundle b = getIntent().getExtras();
        PId = b.getString("PId");

        YouTubePlayerView youTubeView = findViewById(R.id.youtube_view);
        youTubeView.initialize(Constant.arrayListKey.get(0).getKeyName(), YtPlayActivity.this);
    }

    @Override
    public void onInitializationSuccess(Provider provider,YouTubePlayer player, boolean wasRestored) {
        // TODO Auto-generated method stub
        if (!wasRestored) {
            this.player = player;
            player.loadVideo(PId);
        }
    }

    @Override
    protected Provider getYouTubePlayerProvider() {
        // TODO Auto-generated method stub
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }
}
