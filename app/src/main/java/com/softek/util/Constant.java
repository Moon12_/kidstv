package com.softek.util;


import com.softek.item.ItemYTKey;
import com.softek.kidstv.BuildConfig;
import java.io.Serializable;
import java.util.ArrayList;

public class Constant implements Serializable {

    private static final long serialVersionUID = 1L;


    private static String SERVER_URL = BuildConfig.SERVER_URL;

    public static final String API_URL = SERVER_URL + "api.php";

    public static final String IMAGE_PATH = SERVER_URL + "images/";

    public static final String ARRAY_NAME = "YOUTUBE_CHANNEL";

    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_CID = "cid";
    public static final String CATEGORY_IMAGE = "category_image";

    public static final String CHANNEL_ID = "id";
    public static final String CHANNEL_TITLE = "channel_name";
    public static final String CHANNEL_PLAY_URL = "channel_username";
    public static final String CHANNEL_IMAGE = "channel_image";
    public static final String CHANNEL_CAT_NAME = "category_name";

    public static final String APP_NAME = "app_name";
    public static final String APP_IMAGE = "app_logo";
    public static final String APP_VERSION = "app_version";
    public static final String APP_AUTHOR = "app_author";
    public static final String APP_CONTACT = "app_contact";
    public static final String APP_EMAIL = "app_email";
    public static final String APP_WEBSITE = "app_website";
    public static final String APP_DESC = "app_description";
    public static final String APP_PRIVACY_POLICY = "app_privacy_policy";
    public static final String APP_PACKAGE_NAME = "package_name";

    public static final String HOME_CAT_ARRAY = "cat_list";
    public static final String HOME_SLIDER_ARRAY = "slider_list";
    public static final String HOME_FEATURED_ARRAY = "featured_channels";
    public static final String HOME_VIDEO_ARRAY = "latest_channels";

    public static final String VIDEO_ID = "v_id";
    public static final String VIDEO_NAME = "video_title";
    public static final String VIDEO_IMAGE = "video_thumbnail";
    public static final String VIDEO_URL = "video_url";

    public static final String YTPLAYURL = "https://www.googleapis.com/youtube/v3/search?part=snippet,id&order=date&maxResults=20&channelId=";
    public static final String YTAPIKEY = "&key=";
    public static String PLAY_NAME_TITLE;

    public static int AD_COUNT = 0;
    public static int COUNT_KEY = 0;

    public static ArrayList<ItemYTKey> arrayListKey = new ArrayList<>();

    public static final String ADS_BANNER_ID="banner_ad_id";
    public static final String ADS_FULL_ID="interstitial_ad_id";
    public static final String ADS_BANNER_ON_OFF="banner_ad";
    public static final String ADS_FULL_ON_OFF="interstitial_ad";
    public static final String ADS_PUB_ID="publisher_id";
    public static final String ADS_CLICK="interstitial_ad_click";
    public static final String NATIVE_AD_ON_OFF = "native_ad";
    public static final String NATIVE_AD_ID = "native_ad_id";
    public static final String BANNER_TYPE="banner_ad_type";
    public static final String FULL_TYPE="interstitial_ad_type";
    public static final String NATIVE_TYPE="native_ad_type";
    public static boolean isAppUpdate = false, isAppUpdateCancel = false;
    public static int appUpdateVersion;
    public static String  appUpdateUrl, appUpdateDesc,appPrivacyUrl;
    public static String SAVE_ADS_NATIVE_ON_OFF,SAVE_NATIVE_ID,SAVE_BANNER_TYPE,SAVE_FULL_TYPE,SAVE_NATIVE_TYPE,SAVE_NATIVE_CLICK_OTHER,
            SAVE_NATIVE_CLICK_CAT,SAVE_TAG_LINE,SAVE_ADS_BANNER_ID,SAVE_ADS_FULL_ID,SAVE_ADS_BANNER_ON_OFF="false",SAVE_ADS_FULL_ON_OFF="false",SAVE_ADS_PUB_ID,SAVE_ADS_CLICK;


}
