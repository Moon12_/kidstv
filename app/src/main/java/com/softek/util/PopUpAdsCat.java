package com.softek.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.softek.kidstv.CategoryItemActivity;
import com.softek.kidstv.R;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.InterstitialAdListener;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.ixidev.gdpr.GDPRChecker;

public class PopUpAdsCat {

    private static ProgressDialog pDialog;

    public static void ShowInterstitialAds(final Context mContext, final String Id, final String Name) {
        if (Constant.SAVE_ADS_FULL_ON_OFF.equals("true")) {
            if (Constant.SAVE_FULL_TYPE.equals("admob")) {
                Constant.AD_COUNT++;
                if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                    Constant.AD_COUNT = 0;
                    Loading(mContext);
                    final InterstitialAd mInterstitial = new InterstitialAd(mContext);
                    mInterstitial.setAdUnitId(Constant.SAVE_ADS_FULL_ID);
                    GDPRChecker.Request request = GDPRChecker.getRequest();
                    AdRequest.Builder builder = new AdRequest.Builder();
                    if (request == GDPRChecker.Request.NON_PERSONALIZED) {
                        Bundle extras = new Bundle();
                        extras.putString("npa", "1");
                        builder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
                    }
                    mInterstitial.loadAd(builder.build());
                    mInterstitial.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            // TODO Auto-generated method stub
                            super.onAdLoaded();
                            pDialog.dismiss();
                            if (mInterstitial.isLoaded()) {
                                mInterstitial.show();
                            }
                        }

                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            Intent intent = new Intent(mContext, CategoryItemActivity.class);
                            intent.putExtra("Id", Id);
                            intent.putExtra("name", Name);
                            mContext.startActivity(intent);

                        }

                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            pDialog.dismiss();
                            Intent intent = new Intent(mContext, CategoryItemActivity.class);
                            intent.putExtra("Id", Id);
                            intent.putExtra("name", Name);
                            mContext.startActivity(intent);

                        }
                    });
                } else {
                    Intent intent = new Intent(mContext, CategoryItemActivity.class);
                    intent.putExtra("Id", Id);
                    intent.putExtra("name", Name);
                    mContext.startActivity(intent);

                }
            } else {
                Constant.AD_COUNT++;
                if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                    Constant.AD_COUNT = 0;
                    Loading(mContext);
                    final com.facebook.ads.InterstitialAd mInterstitialfb = new com.facebook.ads.InterstitialAd(mContext, Constant.SAVE_ADS_FULL_ID);
                    InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
                        @Override
                        public void onInterstitialDisplayed(Ad ad) {
                        }

                        @Override
                        public void onInterstitialDismissed(Ad ad) {
                            Intent intent = new Intent(mContext, CategoryItemActivity.class);
                            intent.putExtra("Id", Id);
                            intent.putExtra("name", Name);
                            mContext.startActivity(intent);
                         }

                        @Override
                        public void onError(Ad ad, AdError adError) {
                            pDialog.dismiss();
                            Intent intent = new Intent(mContext, CategoryItemActivity.class);
                            intent.putExtra("Id", Id);
                            intent.putExtra("name", Name);
                            mContext.startActivity(intent);

                        }

                        @Override
                        public void onAdLoaded(Ad ad) {
                            pDialog.dismiss();
                            mInterstitialfb.show();
                        }

                        @Override
                        public void onAdClicked(Ad ad) {
                        }

                        @Override
                        public void onLoggingImpression(Ad ad) {
                        }
                    };
                    com.facebook.ads.InterstitialAd.InterstitialLoadAdConfig loadAdConfig = mInterstitialfb.buildLoadAdConfig().withAdListener(interstitialAdListener).withCacheFlags(CacheFlag.ALL).build();
                    mInterstitialfb.loadAd(loadAdConfig);

                } else {
                    Intent intent = new Intent(mContext, CategoryItemActivity.class);
                    intent.putExtra("Id", Id);
                    intent.putExtra("name", Name);
                    mContext.startActivity(intent);

                }
            }
        } else {
            Intent intent = new Intent(mContext, CategoryItemActivity.class);
            intent.putExtra("Id", Id);
            intent.putExtra("name", Name);
            mContext.startActivity(intent);

        }

    }

    private static void Loading(Context mContext) {
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(mContext.getResources().getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.show();
    }
}
