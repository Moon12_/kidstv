package com.softek.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.softek.adapter.FavoriteAdapter;
import com.softek.db.DatabaseHelper;
import com.softek.item.ItemChannelListVideo;
import com.softek.util.ItemOffsetDecoration;
import com.softek.kidstv.R;
import java.util.ArrayList;

public class FavouriteFragment extends Fragment {

    ArrayList<ItemChannelListVideo> mListItem;
    public RecyclerView recyclerView;
    FavoriteAdapter adapter;
    DatabaseHelper databaseHelper;
    TextView text_fav;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.row_fav_recyclerview, container, false);
        setHasOptionsMenu(true);
        mListItem = new ArrayList<>();
        databaseHelper = new DatabaseHelper(getActivity());
        text_fav = rootView.findViewById(R.id.text_fav);
        recyclerView = rootView.findViewById(R.id.vertical_courses_list);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(requireActivity(), R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);

        return rootView;
    }


    private void displayData() {

        adapter = new FavoriteAdapter(getActivity(), mListItem, false);
        recyclerView.setAdapter(adapter);

        if (adapter.getItemCount() == 0) {
            text_fav.setVisibility(View.VISIBLE);
        } else {
            text_fav.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mListItem = databaseHelper.getFavourite();
        displayData();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }
}
