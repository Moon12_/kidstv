package com.softek.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.softek.item.ItemChannelItem;
import com.softek.util.Constant;
import com.softek.kidstv.R;
import com.softek.kidstv.YtPlayActivity;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.InterstitialAdListener;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.ixidev.gdpr.GDPRChecker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ItemRowHolder> {

    private ArrayList<ItemChannelItem> dataList;
    private Context mContext;
    private final int VIEW_ITEM = 1;
    String PlayListId;
    private ProgressDialog pDialog;
    private final int VIEW_PROG = 0;

    public ChannelAdapter(Context context, ArrayList<ItemChannelItem> dataList, RecyclerView recyclerView, String id) {
        this.dataList = dataList;
        this.mContext = context;
        this.PlayListId = id;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_playlist, parent, false);
            return new ContentViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.layout_progressbar, parent, false);
            return new ProgressViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder viewHolder, final int position) {
        if (viewHolder.getItemViewType() == VIEW_ITEM) {
            ContentViewHolder holder = (ContentViewHolder) viewHolder;
            final ItemChannelItem singleItem = dataList.get(position);
            holder.text.setText(singleItem.getPlaylistName());
            Picasso.get().load(singleItem.getPlaylistImageurl()).placeholder(R.drawable.placeholder).into(holder.image);
            holder.text_cat.setText(singleItem.getPlaylistCatName());

            holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constant.SAVE_ADS_FULL_ON_OFF.equals("true")) {
                        if (Constant.SAVE_FULL_TYPE.equals("admob")) {
                            Constant.AD_COUNT++;
                            if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                                Constant.AD_COUNT = 0;
                                Loading();
                                final InterstitialAd mInterstitial = new InterstitialAd(mContext);
                                mInterstitial.setAdUnitId(Constant.SAVE_ADS_FULL_ID);
                                GDPRChecker.Request request = GDPRChecker.getRequest();
                                AdRequest.Builder builder = new AdRequest.Builder();
                                if (request == GDPRChecker.Request.NON_PERSONALIZED) {
                                    Bundle extras = new Bundle();
                                    extras.putString("npa", "1");
                                    builder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
                                }
                                mInterstitial.loadAd(builder.build());
                                mInterstitial.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdLoaded() {
                                        // TODO Auto-generated method stub
                                        super.onAdLoaded();
                                        pDialog.dismiss();
                                        if (mInterstitial.isLoaded()) {
                                            mInterstitial.show();
                                        }
                                    }

                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Intent intent = new Intent(mContext, YtPlayActivity.class);
                                        intent.putExtra("PId", singleItem.getPlaylistId());
                                        mContext.startActivity(intent);
                                    }

                                    @Override
                                    public void onAdFailedToLoad(LoadAdError adError) {
                                        pDialog.dismiss();
                                        Intent intent = new Intent(mContext, YtPlayActivity.class);
                                        intent.putExtra("PId", singleItem.getPlaylistId());
                                        mContext.startActivity(intent);
                                    }
                                });
                            } else {
                                Intent intent = new Intent(mContext, YtPlayActivity.class);
                                intent.putExtra("PId", singleItem.getPlaylistId());
                                mContext.startActivity(intent);
                            }
                        } else {
                            Constant.AD_COUNT++;
                            if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                                Constant.AD_COUNT = 0;
                                Loading();
                                final com.facebook.ads.InterstitialAd mInterstitialfb = new com.facebook.ads.InterstitialAd(mContext, Constant.SAVE_ADS_FULL_ID);
                                InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
                                    @Override
                                    public void onInterstitialDisplayed(Ad ad) {
                                    }

                                    @Override
                                    public void onInterstitialDismissed(Ad ad) {
                                        Intent intent = new Intent(mContext, YtPlayActivity.class);
                                        intent.putExtra("PId", singleItem.getPlaylistId());
                                        mContext.startActivity(intent);
                                    }

                                    @Override
                                    public void onError(Ad ad, AdError adError) {
                                        pDialog.dismiss();
                                        Intent intent = new Intent(mContext, YtPlayActivity.class);
                                        intent.putExtra("PId", singleItem.getPlaylistId());
                                        mContext.startActivity(intent);
                                    }

                                    @Override
                                    public void onAdLoaded(Ad ad) {
                                        pDialog.dismiss();
                                        mInterstitialfb.show();
                                    }

                                    @Override
                                    public void onAdClicked(Ad ad) {
                                    }

                                    @Override
                                    public void onLoggingImpression(Ad ad) {
                                    }
                                };
                                com.facebook.ads.InterstitialAd.InterstitialLoadAdConfig loadAdConfig = mInterstitialfb.buildLoadAdConfig().withAdListener(interstitialAdListener).withCacheFlags(CacheFlag.ALL).build();
                                mInterstitialfb.loadAd(loadAdConfig);

                            } else {
                                Intent intent = new Intent(mContext, YtPlayActivity.class);
                                intent.putExtra("PId", singleItem.getPlaylistId());
                                mContext.startActivity(intent);
                            }
                        }
                    } else {
                        Intent intent = new Intent(mContext, YtPlayActivity.class);
                        intent.putExtra("PId", singleItem.getPlaylistId());
                        mContext.startActivity(intent);
                    }
                }
            });
            holder.imageShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, singleItem.getPlaylistName() + "\n" + "https://www.youtube.com/watch?v=" + singleItem.getPlaylistId() + "\n" + mContext.getString(R.string.share_msg) + "\n" + "https://play.google.com/store/apps/details?id=" + mContext.getPackageName());
                    sendIntent.setType("text/plain");
                    mContext.startActivity(sendIntent);
                }
            });
        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() + 1 : 0);
    }

    public void hideHeader() {
        ProgressViewHolder.progressBar.setVisibility(View.GONE);
    }

    private boolean isHeader(int position) {
        return position == dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? VIEW_PROG : VIEW_ITEM;
    }

    static class ItemRowHolder extends RecyclerView.ViewHolder {
        ItemRowHolder(View itemView) {
            super(itemView);
        }
    }

    class ContentViewHolder extends ItemRowHolder {
        ImageView image, imageShare;
        TextView text, text_cat;
        LinearLayout lyt_parent;

        ContentViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.text);
            lyt_parent = itemView.findViewById(R.id.rootLayout);
            imageShare = itemView.findViewById(R.id.imageShare);
            text_cat = itemView.findViewById(R.id.text_cat);
        }
    }

    static class ProgressViewHolder extends ItemRowHolder {
        static ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }

    private void Loading() {
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(mContext.getResources().getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.show();
    }
}
