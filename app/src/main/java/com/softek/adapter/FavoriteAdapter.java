package com.softek.adapter;

import android.content.ContentValues;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.softek.db.DatabaseHelper;
import com.softek.item.ItemChannelListVideo;
import com.softek.util.Constant;
import com.softek.util.PopUpAds;
import com.softek.kidstv.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ItemRowHolder> {

    private ArrayList<ItemChannelListVideo> dataList;
    private Context mContext;
    private boolean isVisible;
    private DatabaseHelper databaseHelper;

    public FavoriteAdapter(Context context, ArrayList<ItemChannelListVideo> dataList, boolean flag) {
        this.dataList = dataList;
        this.mContext = context;
        this.isVisible = flag;
        databaseHelper = new DatabaseHelper(mContext);
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ca_list_item, parent, false);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {
        final ItemChannelListVideo singleItem = dataList.get(position);
        holder.text.setText(singleItem.getChannelName());
        Picasso.get().load(singleItem.getChannelImage()).placeholder(R.drawable.placeholder).into(holder.image);

        if (databaseHelper.getFavouriteById(String.valueOf(singleItem.getChannelId()))) {
            holder.imageFavourite.setImageResource(R.drawable.ic_favourite_hover);
        } else {
            holder.imageFavourite.setImageResource(R.drawable.ic_favourite_1);
        }

        holder.imageFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues fav = new ContentValues();
                if (databaseHelper.getFavouriteById(String.valueOf(singleItem.getChannelId()))) {
                    databaseHelper.removeFavouriteById(String.valueOf(singleItem.getChannelId()));
                    holder.imageFavourite.setImageResource(R.drawable.ic_favourite_1);
                    Toast.makeText(mContext, mContext.getString(R.string.favourite_remove), Toast.LENGTH_SHORT).show();
                    dataList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, dataList.size());
                } else {
                    fav.put(DatabaseHelper.KEY_ID, String.valueOf(singleItem.getChannelId()));
                    fav.put(DatabaseHelper.KEY_TITLE, singleItem.getChannelName());
                    fav.put(DatabaseHelper.KEY_IMAGE, singleItem.getChannelImage());
                    fav.put(DatabaseHelper.KEY_PLAYLIST, singleItem.getChannelPlayUrl());
                    databaseHelper.addFavourite(DatabaseHelper.TABLE_FAVOURITE_NAME, fav, null);
                    holder.imageFavourite.setImageResource(R.drawable.ic_favourite_hover);
                    Toast.makeText(mContext, mContext.getString(R.string.favourite_add), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.PLAY_NAME_TITLE = singleItem.getChannelCatName();
                PopUpAds.ShowInterstitialAds(mContext, singleItem.getChannelPlayUrl(), singleItem.getChannelName());
             }
        });


    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    class ItemRowHolder extends RecyclerView.ViewHolder {
        ImageView image, imageFavourite;
        TextView text;
        LinearLayout lyt_parent;

        ItemRowHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageFavourite = itemView.findViewById(R.id.imageFavourite);
            text = itemView.findViewById(R.id.text);
            lyt_parent = itemView.findViewById(R.id.rootLayout);
        }
    }
}
