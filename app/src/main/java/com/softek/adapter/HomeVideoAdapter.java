package com.softek.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.softek.item.ItemVideo;
import com.softek.util.Constant;
import com.softek.kidstv.R;
import com.softek.kidstv.YoutubePlay;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.InterstitialAdListener;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.ixidev.gdpr.GDPRChecker;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class HomeVideoAdapter extends RecyclerView.Adapter<HomeVideoAdapter.ItemRowHolder> {

    private ArrayList<ItemVideo> dataList;
    private Context mContext;
    private ProgressDialog pDialog;

    public HomeVideoAdapter(Context context, ArrayList<ItemVideo> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_video_item, parent, false);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {
        final ItemVideo singleItem = dataList.get(position);
        holder.text.setText(singleItem.getVideoName());
        Picasso.get().load(singleItem.getVideoImage()).placeholder(R.drawable.placeholder).into(holder.image);

        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constant.SAVE_ADS_FULL_ON_OFF.equals("true")) {
                    if (Constant.SAVE_FULL_TYPE.equals("admob")) {
                        Constant.AD_COUNT++;
                        if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                            Constant.AD_COUNT = 0;
                            Loading();
                            final InterstitialAd mInterstitial = new InterstitialAd(mContext);
                            mInterstitial.setAdUnitId(Constant.SAVE_ADS_FULL_ID);
                            GDPRChecker.Request request = GDPRChecker.getRequest();
                            AdRequest.Builder builder = new AdRequest.Builder();
                            if (request == GDPRChecker.Request.NON_PERSONALIZED) {
                                Bundle extras = new Bundle();
                                extras.putString("npa", "1");
                                builder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
                            }
                            mInterstitial.loadAd(builder.build());
                            mInterstitial.setAdListener(new AdListener() {
                                @Override
                                public void onAdLoaded() {
                                    // TODO Auto-generated method stub
                                    super.onAdLoaded();
                                    pDialog.dismiss();
                                    if (mInterstitial.isLoaded()) {
                                        mInterstitial.show();
                                    }
                                }

                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Intent i = new Intent(mContext, YoutubePlay.class);
                                    i.putExtra("id", singleItem.getVideoUrl());
                                    mContext.startActivity(i);
                                }

                                @Override
                                public void onAdFailedToLoad(LoadAdError adError) {
                                    pDialog.dismiss();
                                    Intent i = new Intent(mContext, YoutubePlay.class);
                                    i.putExtra("id", singleItem.getVideoUrl());
                                    mContext.startActivity(i);
                                }
                            });
                        } else {
                            Intent i = new Intent(mContext, YoutubePlay.class);
                            i.putExtra("id", singleItem.getVideoUrl());
                            mContext.startActivity(i);
                        }
                    } else {
                        Constant.AD_COUNT++;
                        if (Constant.AD_COUNT == Integer.parseInt(Constant.SAVE_ADS_CLICK)) {
                            Constant.AD_COUNT = 0;
                            Loading();
                            final com.facebook.ads.InterstitialAd mInterstitialfb = new com.facebook.ads.InterstitialAd(mContext, Constant.SAVE_ADS_FULL_ID);
                            InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
                                @Override
                                public void onInterstitialDisplayed(Ad ad) {
                                }

                                @Override
                                public void onInterstitialDismissed(Ad ad) {
                                    Intent i = new Intent(mContext, YoutubePlay.class);
                                    i.putExtra("id", singleItem.getVideoUrl());
                                    mContext.startActivity(i);
                                }

                                @Override
                                public void onError(Ad ad, AdError adError) {
                                    pDialog.dismiss();
                                    Intent i = new Intent(mContext, YoutubePlay.class);
                                    i.putExtra("id", singleItem.getVideoUrl());
                                    mContext.startActivity(i);
                                }

                                @Override
                                public void onAdLoaded(Ad ad) {
                                    pDialog.dismiss();
                                    mInterstitialfb.show();
                                }

                                @Override
                                public void onAdClicked(Ad ad) {
                                }

                                @Override
                                public void onLoggingImpression(Ad ad) {
                                }
                            };
                            com.facebook.ads.InterstitialAd.InterstitialLoadAdConfig loadAdConfig = mInterstitialfb.buildLoadAdConfig().withAdListener(interstitialAdListener).withCacheFlags(CacheFlag.ALL).build();
                            mInterstitialfb.loadAd(loadAdConfig);

                        } else {
                            Intent i = new Intent(mContext, YoutubePlay.class);
                            i.putExtra("id", singleItem.getVideoUrl());
                            mContext.startActivity(i);
                        }
                    }
                } else {
                    Intent i = new Intent(mContext, YoutubePlay.class);
                    i.putExtra("id", singleItem.getVideoUrl());
                    mContext.startActivity(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    class ItemRowHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView text;
        LinearLayout lyt_parent;

        ItemRowHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.text);
            lyt_parent = itemView.findViewById(R.id.rootLayout);
        }
    }

    private void Loading() {
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(mContext.getResources().getString(R.string.loading));
        pDialog.setCancelable(false);
        pDialog.show();
    }
}
